#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

mod uart;

use crate::uart::SendCount;

use core::sync::atomic::{AtomicU32, Ordering};
use defmt::*;
use embassy_executor::Spawner;
use embassy_rp::bind_interrupts;
use embassy_rp::gpio::{AnyPin, Input, Level, Output, Pin, Pull};
use embassy_rp::peripherals::*;
use embassy_rp::uart::{Config, InterruptHandler, Uart};
use embassy_time::{Duration, Timer};
use {defmt_rtt as _, panic_probe as _};

static COUNTER: AtomicU32 = AtomicU32::new(0);

#[embassy_executor::task]
async fn count_pwm(pin: AnyPin) {
    let mut button = Input::new(pin, Pull::Up);

    loop {
        button.wait_for_falling_edge().await;
        let count = COUNTER.load(Ordering::Acquire);
        COUNTER.store(count + 1, Ordering::Release);
    }
}

#[embassy_executor::task]
async fn flash_light(button_pin: AnyPin, led_pin: AnyPin) {
    let mut button = Input::new(button_pin, Pull::Down);
    let mut led = Output::new(led_pin, Level::Low);

    led.set_high();
    Timer::after(Duration::from_millis(500)).await;
    led.set_low();

    loop {
        button.wait_for_rising_edge().await;
        let count = COUNTER.load(Ordering::Acquire);
        COUNTER.store(0, Ordering::Release);
        for _ in 0..count {
            info!("LED on!");
            led.set_high();
            Timer::after(Duration::from_millis(200)).await;

            info!("LED off!");
            led.set_low();
            Timer::after(Duration::from_millis(200)).await;
        }
    }
}

bind_interrupts!(struct Uart0Irq {
    UART0_IRQ => InterruptHandler<UART0>;
});

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    let p = embassy_rp::init(Default::default());

    let mut config = Config::default();
    config.baudrate = 9600;
    let uart0 = Uart::new(
        p.UART0, p.PIN_12, p.PIN_13, Uart0Irq, p.DMA_CH0, p.DMA_CH1, config,
    );

    spawner.spawn(count_pwm(p.PIN_0.degrade())).unwrap();
    spawner
        .spawn(flash_light(p.PIN_1.degrade(), p.PIN_25.degrade()))
        .unwrap();
    SendCount::new(&COUNTER).spawn(&spawner, uart0);
}
