Unless you get a second pico in which case you could use pico-probe, run it with:

cargo install elf2uf2-rs
cargo build --release
elf2uf2-rs target/thumbv6m-none-eabi/release/flow-meter flow-meter

Press BOOTSEL and plug the pico in
Drag and drop flow-meter.uf2 into the RPI-RP2 folder
