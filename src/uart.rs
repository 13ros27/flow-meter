use core::sync::atomic::{AtomicU32, Ordering};
use embassy_executor::Spawner;
use embassy_rp::peripherals::*;
use embassy_rp::uart::{Async, Uart};

pub struct SendCount {
    counter: &'static AtomicU32,
}

impl SendCount {
    pub fn new(counter: &'static AtomicU32) -> Self {
        Self { counter }
    }

    pub fn spawn(self, spawner: &Spawner, uart: Uart<'static, UART0, Async>) {
        spawner.spawn(main(self, uart)).unwrap();
    }
}

#[embassy_executor::task]
async fn main(this: SendCount, mut uart: Uart<'static, UART0, Async>) {
    loop {
        let mut buffer: [u8; 1] = [0];
        uart.read(&mut buffer).await.unwrap();
        if buffer[0] == 0 {
            continue;
        }

        let mut ret = [0; 10];
        let mut count = this.counter.load(Ordering::Relaxed);
        for i in 0..10 {
            let scale = 10_u32.pow(9 - i);
            let num = count / scale;
            count -= num * scale;
            ret[i as usize] = (num + 30) as u8;
        }

        uart.write(&ret).await.unwrap();
        uart.blocking_flush().unwrap();
    }
}
